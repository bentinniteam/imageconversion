# Image Conversion

This repo contains the Benchmark tests around various implementations
of an image conversion client that converts
image file formats (*e.g.*, PNG) into JPEG.

## ImageConversion.Benchmark

Collection of benchmark tests for performance comparison between different
image conversion libraries:

- [SkiaSharp](https://github.com/mono/SkiaSharp)
- [ImageSharp](https://github.com/SixLabors/ImageSharp)

## Development workflow

**Prequisites:** Docker, .NET 6

Tests can be be developed and ran using Visual Studio 2022 and should be built in `Release` mode and ran without Debugging as per [BenchmarkDotNet](https://github.com/dotnet/BenchmarkDotNet) reqs.

### Profiles

There's 2 VS profiles to run the tests:

 * `ImageConversion.Benchmark`
    * test results running from one's machine for quick dev feedback loop
    * you can choose via stdin which benchmark to run (quickest is ImageProcessor)
    * test output written to: `bin/Release/net6.0/BenchmarkDotNet.Artifacts/results`
 * `Docker`
    * test results running in a container to reproduce the target deployment env for accurate results
    * benchmark test is set by default and changeable via `launchSettings.json` (this is because VS Output window doesn't allow input)
    * test output written to: `BenchmarkDotNet.Artifacts/FromDocker`

### Console
You can also run the tests from inside the benchmark project folder by running:

```bash
# test output written to: BenchmarkDotNet.Artifacts/results.
$ dotnet run -c Release
```

### Resources

The `resources` folder contains the images used in the benchmark tests.

The `resources` folder is in the repo's root to be used by a collection of other tests covered in each repository project. 

## Why I am fond of this code

This code allowed me to benchmark different code implementations, as well as it guided me to find the final implementation based on the benchmark results.

After initial test results, it pointed me to prefer the `SkiaSharp` library as it had the best performance/memory ratio. I've kept iterating on my image conversion client implementation and benchmarked different ways of using `SkiaSharp` until I was happy with the results in terms of memory usage.


### Initial Test Results

``` ini
BenchmarkDotNet=v0.12.1, OS=debian 11 (container)
Intel Core i7-9750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET Core SDK=6.0.101
  [Host]     : .NET Core 6.0.1 (CoreCLR 6.0.121.56705, CoreFX 6.0.121.56705), X64 RyuJIT
  DefaultJob : .NET Core 6.0.1 (CoreCLR 6.0.121.56705, CoreFX 6.0.121.56705), X64 RyuJIT

|     Method |  FileName |     Mean |    Error |   StdDev |    Gen 0 |    Gen 1 |    Gen 2 |  Allocated |
|----------- |---------- |---------:|---------:|---------:|---------:|---------:|---------:|-----------:|
|  SkiaSharp | jfif.jfif | 13.50 ms | 0.268 ms | 0.648 ms |  15.6250 |        - |        - |  133.65 KB |
| ImageSharp | jfif.jfif | 10.46 ms | 0.090 ms | 0.080 ms | 328.1250 | 312.5000 | 312.5000 | 2199.12 KB |
```

### Different SkiaSharp implementations

``` ini
BenchmarkDotNet=v0.12.1, OS=debian 11 (container)
Intel Core i7-9750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET Core SDK=6.0.101
  [Host]     : .NET Core 6.0.1 (CoreCLR 6.0.121.56705, CoreFX 6.0.121.56705), X64 RyuJIT
  DefaultJob : .NET Core 6.0.1 (CoreCLR 6.0.121.56705, CoreFX 6.0.121.56705), X64 RyuJIT

|                              Method |  FileName |     Mean |    Error |   StdDev |   Median | Ratio | RatioSD |   Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------------ |---------- |---------:|---------:|---------:|---------:|------:|--------:|--------:|------:|------:|----------:|
|                      SkiaSharpAsync | jfif.jfif | 13.34 ms | 0.363 ms | 1.070 ms | 13.06 ms |  1.00 |    0.00 | 31.2500 |     - |     - | 207.85 KB |
|            SkiaSharpAsyncMemoryPool | jfif.jfif | 13.90 ms | 0.363 ms | 1.049 ms | 13.90 ms |  1.05 |    0.11 | 15.6250 |     - |     - | 128.72 KB |
| SkiaSharpAsyncRecycableMemoryStream | jfif.jfif | 14.07 ms | 0.394 ms | 1.155 ms | 13.76 ms |  1.06 |    0.12 |       - |     - |     - |   3.76 KB |
```