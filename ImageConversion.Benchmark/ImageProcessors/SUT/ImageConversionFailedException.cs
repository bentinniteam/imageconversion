namespace ImageConversion.Benchmark.ImageProcessors.SUT;

public class ImageConversionFailedException : Exception
{
    public ImageConversionFailedException(string msg) : base(msg) {}
}
